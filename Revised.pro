# Revised, a visual editor for Ren'Py
# Copyright 2012-2016  JanKusanagi JRR <jancoding@gmx.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
# -------------------------------------------------
# Project created by QtCreator
# -------------------------

message("Generating Makefile for Revised... $$escape_expand(\\n)\
Using $$_FILE_$$escape_expand(\\n)")


QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4) {
    message("Building with Qt 5: v$$QT_VERSION $$escape_expand(\\n)")
    warning(">> To build with Qt 4, you probably need to use qmake-qt4 instead\
$$escape_expand(\\n)")
    QT += widgets
    #QT += phonon4qt5
}

lessThan(QT_MAJOR_VERSION, 5) {
    message("Building with Qt 4: v$$QT_VERSION")
    warning(">> To build with Qt 5, you might need to use qmake-qt5 instead\
$$escape_expand(\\n)")
    ### When using Qt 4, if I end up using Phonon for something...
    #QT += phonon
}


TARGET = revised
TEMPLATE = app


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/scenedesigner.cpp \
    src/codeeditor.cpp \
    src/configurationwindow.cpp \
    src/chapterlist.cpp \
    src/scenepreviewer.cpp \
    src/charactermanager.cpp \
    src/charactercombobox.cpp \
    src/imagemanager.cpp \
    src/chapter.cpp \
    src/step.cpp \
    src/gameoptionsmanager.cpp \
    src/choicemenueditor.cpp

HEADERS  += src/mainwindow.h \
    src/scenedesigner.h \
    src/codeeditor.h \
    src/configurationwindow.h \
    src/chapterlist.h \
    src/scenepreviewer.h \
    src/charactermanager.h \
    src/charactercombobox.h \
    src/imagemanager.h \
    src/chapter.h \
    src/step.h \
    src/gameoptionsmanager.h \
    src/choicemenueditor.h


TRANSLATIONS += translations/revised_es.ts \
                translations/revised_de.ts \
                translations/revised_ca.ts \
                translations/revised_fr.ts \
                translations/revised_it.ts \
                translations/revised_EMPTY.ts

OTHER_FILES += \
    LICENSE \
    CHANGELOG \
    INSTALL \
    README \
    TODO \
    revised.desktop

RESOURCES += \
    revised.qrc


message("$$escape_expand(\\n\\n\\n)\
Makefile done!$$escape_expand(\\n\\n)\
If you're building the binary, you can run 'make' now. $$escape_expand(\\n)")
