<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ChapterList</name>
    <message>
        <location filename="../src/chapterlist.cpp" line="31"/>
        <source>Chapter List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chapterlist.cpp" line="41"/>
        <source>List of chapters in your game, with their steps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/chapterlist.cpp" line="124"/>
        <location filename="../src/chapterlist.cpp" line="262"/>
        <source>Chapter %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CharacterComboBox</name>
    <message>
        <location filename="../src/charactercombobox.cpp" line="57"/>
        <source>Narrator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactercombobox.cpp" line="75"/>
        <source>Add New Character</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CharacterManager</name>
    <message>
        <location filename="../src/charactermanager.cpp" line="26"/>
        <source>Character Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="50"/>
        <source>Character Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="52"/>
        <source>&amp;Alias</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="58"/>
        <source>&amp;Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="64"/>
        <source>&amp;Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="71"/>
        <source>&amp;Pick a Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="77"/>
        <source>&amp;Update Character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="83"/>
        <source>Add &amp;More Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="88"/>
        <source>&amp;Remove Selected Character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="425"/>
        <source>Choose the color for this player&apos;s name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/charactermanager.cpp" line="503"/>
        <location filename="../src/charactermanager.cpp" line="504"/>
        <source>New Character</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChoiceMenuEditor</name>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="25"/>
        <source>Add Choice Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="36"/>
        <source>Text shown for this option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="42"/>
        <source>&amp;Add this choice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="46"/>
        <source>&amp;Remove selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="52"/>
        <source>&amp;Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="57"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="69"/>
        <source>&amp;Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="70"/>
        <source>&amp;Jump to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="74"/>
        <source>C&amp;hoices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="154"/>
        <source>Error: No choices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/choicemenueditor.cpp" line="155"/>
        <source>There are no choices in the menu.
You need at least one.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigurationWindow</name>
    <message>
        <location filename="../src/configurationwindow.cpp" line="27"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configurationwindow.cpp" line="37"/>
        <source>Executable with path, like /usr/games/renpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configurationwindow.cpp" line="41"/>
        <source>Default folder for new projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configurationwindow.cpp" line="45"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configurationwindow.cpp" line="51"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configurationwindow.cpp" line="63"/>
        <source>Ren&apos;Py executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configurationwindow.cpp" line="65"/>
        <source>Default project folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GameOptionsManager</name>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="25"/>
        <source>Game Options Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="36"/>
        <source>Title of the game&apos;s window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="38"/>
        <source>Width of the game&apos;s window. 800, 1024, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="40"/>
        <source>Hight of the game&apos;s window. 600, 768, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="50"/>
        <source>Window &amp;title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="51"/>
        <source>Screen &amp;width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="52"/>
        <source>Screen &amp;height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="57"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gameoptionsmanager.cpp" line="61"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageManager</name>
    <message>
        <location filename="../src/imagemanager.cpp" line="25"/>
        <source>Image Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="48"/>
        <source>Add &amp;More Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="55"/>
        <source>&amp;ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="59"/>
        <source>Example: character happy smile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="71"/>
        <source>Re&amp;name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="76"/>
        <source>&amp;Remove from List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="83"/>
        <source>Set as &amp;Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="89"/>
        <source>&amp;Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="94"/>
        <source>&amp;Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="99"/>
        <source>&amp;Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="115"/>
        <source>&amp;Available images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="122"/>
        <source>Insert image at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="130"/>
        <source>&amp;Selected image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="373"/>
        <source>Select one or more images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/imagemanager.cpp" line="375"/>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="30"/>
        <source>Visual Editor for Ren&apos;Py</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="59"/>
        <source>&amp;Visual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="61"/>
        <source>&amp;Code Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>&amp;New Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="124"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="130"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="137"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="141"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="148"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Exit Revised</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="156"/>
        <source>Open &amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="161"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>&amp;Run Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="181"/>
        <source>Run the game in Ren&apos;Py</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="186"/>
        <source>Define &amp;Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <source>Define alias for characters in the game, with their name color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>Define &amp;Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="197"/>
        <source>Define the images used in the game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="202"/>
        <source>Define Game &amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>Define general game parameters like resolution or window title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="210"/>
        <source>&amp;Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="223"/>
        <source>Set &amp;Background Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="226"/>
        <source>Add &amp;Character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="229"/>
        <source>Add &amp;Dialog Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="232"/>
        <source>&amp;Scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <source>&amp;Configure Revised</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="244"/>
        <source>General program configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="248"/>
        <source>S&amp;ettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="257"/>
        <source>Visit &amp;Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="263"/>
        <source>&amp;Ren&apos;Py Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="265"/>
        <source>Go to Ren&apos;Py&apos;s Documentation on the web</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="271"/>
        <source>&amp;About Revised...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="276"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <source>Not a Ren&apos;Py project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="398"/>
        <source>The folder %1 does not seem to contain a Ren&apos;Py project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="431"/>
        <source>Project %1 loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="492"/>
        <source>Select the folder where the project will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="508"/>
        <source>Project name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="509"/>
        <source>Type a name for your project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="520"/>
        <source>Project already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="521"/>
        <source>There is a project named %1 at %2 already.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="585"/>
        <source>Select the project folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="618"/>
        <source>Saving project...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="648"/>
        <location filename="../src/mainwindow.cpp" line="665"/>
        <source>No project loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="666"/>
        <source>There is no project currently loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="680"/>
        <source>Cannot execute Ren&apos;Py</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="681"/>
        <source>Ren&apos;Py can&apos;t be run.
Make sure you have it correctly installed, and the correct path is set in the configuration.

Currently set to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="722"/>
        <source>About Revised</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="729"/>
        <source>Revised is a visual editor, or wizard, for Ren&apos;Py, the visual-novel game engine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="737"/>
        <source>Ren&apos;Py has a lot of capabilities, and Revised&apos;s purpose is not to allow everything that Ren&apos;Py can do, just some of the basics.&lt;br /&gt;At least for now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="742"/>
        <source>You can think of it as a step-by-step wizard to create the skeleton of a Ren&apos;Py game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="749"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your corresponding language and name ;)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="757"/>
        <source>Revised is Free Software under a &lt;b&gt;GNU GPL&lt;/b&gt; license.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SceneDesigner</name>
    <message>
        <location filename="../src/scenedesigner.cpp" line="37"/>
        <source>&amp;Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="42"/>
        <source>&amp;Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="50"/>
        <source>&amp;Delete this step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="57"/>
        <source>Insert Choice &amp;Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="58"/>
        <source>Insert a menu with several choices. Each choice will go to a different part of the game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="65"/>
        <source>Set &amp;Background...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="73"/>
        <source>Insert &amp;Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="92"/>
        <source>Some dialog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scenedesigner.cpp" line="97"/>
        <source>Insert &amp;Text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Step</name>
    <message>
        <location filename="../src/step.cpp" line="42"/>
        <source>Label: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="51"/>
        <source>Scene Change: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="60"/>
        <source>Image: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="69"/>
        <source>Menu: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="86"/>
        <source>Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="94"/>
        <source>Play Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="101"/>
        <source>Stop Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="108"/>
        <source>Play Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="115"/>
        <source>Stop Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="162"/>
        <source>Narration: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="166"/>
        <source>Dialog: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/step.cpp" line="174"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
